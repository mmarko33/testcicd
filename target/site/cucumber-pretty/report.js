$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/region/ListRegion.feature");
formatter.feature({
  "name": "Listagem Regiões",
  "description": "\tComo usuário do ambiente Edenred\n\tQuero pesquisar uma região\n\tPara que este tenha seus dados atualizados na Plataforma de Manutenção.",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@lr"
    }
  ]
});
formatter.background({
  "name": "Ingressar na tela de listagem de regioes",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "ingresso na tela de listagem de regioes",
  "keyword": "Given "
});
formatter.match({
  "location": "br.com.americas.steps.ListRegionSteps.ingressoNaTelaDeListagemDeRegioes()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Verificar funcao scroll",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@lr"
    }
  ]
});
formatter.step({
  "name": "eu faço scroll dentro da tabela de regioes",
  "keyword": "When "
});
formatter.match({
  "location": "br.com.americas.steps.ListRegionSteps.euFaçoScrollDentroDaTabelaDeRegioes()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "mostra a seguinte pagina de resultados de regioes embaixo da atual",
  "keyword": "Then "
});
formatter.match({
  "location": "br.com.americas.steps.ListRegionSteps.mostraASeguintePaginaDeResultadosDeRegioesEmbaixoDaAtual()"
});
formatter.result({
  "status": "passed"
});
formatter.embedding("image/png", "embedded0.png", null);
formatter.after({
  "status": "passed"
});
});