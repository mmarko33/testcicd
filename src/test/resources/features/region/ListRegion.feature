@lr
Feature: Listagem Regiões
	Como usuário do ambiente Edenred
	Quero pesquisar uma região
	Para que este tenha seus dados atualizados na Plataforma de Manutenção.
  
  Background: Ingressar na tela de listagem de regioes
  	Given ingresso na tela de listagem de regioes


  Scenario: Verificar funcao scroll
  	When eu faço scroll dentro da tabela de regioes
  	Then mostra a seguinte pagina de resultados de regioes embaixo da atual