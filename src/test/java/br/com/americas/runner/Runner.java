package br.com.americas.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
 features = "src/test/resources/features/",
 glue= {"/br/com/americas/steps","/br/com/americas/runner"},
 plugin = { "pretty",
		    "html:target/site/cucumber-pretty",
		    "json:target/site/cucumber.json"
 		  },
dryRun = false,
strict = true,
snippets = CucumberOptions.SnippetType.CAMELCASE,
tags ={"@lr"})
 
public class Runner {

}