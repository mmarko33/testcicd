package br.com.americas.runner;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;


public class ClassInit {

	
    public static WebDriver driver;


    @Before
    public void initializeTest() {
        System.setProperty("webdriver.chrome.driver", getPathChromeDriver());
        //System.setProperty("webdriver.gecko.driver", getPathFirefoxDriver());
        driver = new ChromeDriver(addOptions());
        //WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @After
    public void endTest(Scenario scenario) {
		try {
			final byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png");
			System.out.println("Starting - " + scenario.getName());
			System.out.println(scenario.getName() + " Status - " + scenario.getStatus());
			} 
			catch (WebDriverException e) {
				e.printStackTrace();
			}
	   
			driver.quit();

    }
    
    public static String getPathChromeDriver(){
        String so = System.getProperty("os.name").toLowerCase();
        so = so.replaceAll(" ", "");
        so = so.replaceAll("[0-9]", "");
        String directory = System.getProperty("user.dir");

        if("windows".equals(so)){
          return directory + "/drivers/windows/chromedriver.exe";
        } else {
          return directory + String.format("/drivers/%s/chromedriver",so);
        }
      }

      static String getPathFirefoxDriver(){
        String so = System.getProperty("os.name").toLowerCase();
        so = so.replaceAll(" ", "");
        so = so.replaceAll("[0-9]", "");
        String directory = System.getProperty("user.dir");

        if("windows".equals(so)){
          return directory + "/drivers/windows/geckodriver.exe";
        } else {
          return directory + String.format("/drivers/%s/geckodriver",so);
        }
      }
      
      static ChromeOptions addOptions(){
    	    String so = System.getProperty("os.name").toLowerCase();
    	    so = so.replaceAll(" ", "");
    	    so = so.replaceAll("[0-9]", "");
    	    ChromeOptions options = new ChromeOptions();
    	    
    	    if("linux".equals(so)){
    	    	options.addArguments("--headless");
    	        options.addArguments("--no-sandbox");
    	    }
    	    
    		return options;
    	}
    
}
