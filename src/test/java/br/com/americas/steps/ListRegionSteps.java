package br.com.americas.steps;


import br.com.americas.pages.ListRegionObjects;
import br.com.americas.runner.ClassInit;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ListRegionSteps {
	
	ListRegionObjects lro;
	
	@Given("ingresso na tela de listagem de regioes")
	public void ingressoNaTelaDeListagemDeRegioes() throws InterruptedException {
		lro = new ListRegionObjects(ClassInit.driver);
		lro.open_url();
	}


	@When("eu faço scroll dentro da tabela de regioes")
	public void euFaçoScrollDentroDaTabelaDeRegioes() throws InterruptedException {
		System.out.println("Scroll");
	}

	@Then("mostra a seguinte pagina de resultados de regioes embaixo da atual")
	public void mostraASeguintePaginaDeResultadosDeRegioesEmbaixoDaAtual() {
		System.out.println("Scroll");
	}

}
