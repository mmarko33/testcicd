package br.com.americas.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ProfileUtil {

    public static String getCurrentProfile() {

        try {
            Properties info = new Properties();
            InputStream is = ProfileUtil.class.getResourceAsStream("/config.properties");
            info.load(is);

            //System.getenv("URL_BASE_AMERICAS");
            return info.getProperty("URL_BASE_AMERICAS");

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;

    }
}

