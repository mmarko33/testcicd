package br.com.americas.utils;


import java.io.File;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


public class MethodsUtil
{		
	   WebDriver driver;
	
	   public MethodsUtil(WebDriver mydriver) 
	   {
		   this.driver = mydriver;
	   }
	   
	   public WebElement get_element(String type,String value)
	   {	
		   	WebElement elem;
	        type = type.toLowerCase();
	        if(type == "xpath"){
				elem = this.driver.findElement(By.xpath(value));
				System.out.println("Element found");
				return elem;
			}
			else if(type == "id"){	
				elem = this.driver.findElement(By.id(value));
				System.out.println("Element found");
				return elem;
				}
				else if(type == "css"){	
					elem = this.driver.findElement(By.cssSelector(value));
					System.out.println("Element found");
					return elem;
					}	
					else if(type == "partiallinktext"){	
						elem = this.driver.findElement(By.partialLinkText(value));
						System.out.println("Element found");
						return elem;
						}
						else {
							elem = this.driver.findElement(By.partialLinkText(value));
							System.out.println("Element found");
							return elem;
						}
	   }
	
	   public void send_message(String type,String value,String message) throws InterruptedException
	   {
		   WebElement elem;
	       elem = get_element(type,value);
	       try {
	    	   elem.sendKeys(message);
	    	   Thread.sleep(2500);
	           System.out.println("The message was send");
	       }
	       catch (NoSuchElementException e) {
	    	   System.out.println("The message was not send"); 
	       }
	   }
	   
	   public void clear_element(String type,String value) throws InterruptedException
	   {
		   WebElement elem;
	       elem = get_element(type,value);
	       try {
	    	   elem.clear();
	    	   Thread.sleep(2500);
	           System.out.println("The field was clear");
	       }
	       catch (NoSuchElementException e) {
	    	   System.out.println("The field was not clear"); 
	       }
	   }
	   
	   public void send_keyboard(String type,String value,Keys key) throws InterruptedException
	   {
		   WebElement elem;
	       elem = get_element(type,value);
	       try {
	    	   elem.sendKeys(key);
	    	   Thread.sleep(2500);
	           System.out.println("The message was send");
	       }
	       catch (NoSuchElementException e) {
	    	   System.out.println("The message was not send"); 
	       }
	   }
	   
	   public void click(String type,String value) throws InterruptedException
	   {
		   WebElement elem;
	       elem = get_element(type,value);
	       try {
	    	   elem.click();
	    	   Thread.sleep(2500);
	           System.out.println("The element was clicked with sucess");
	       }
	       catch (NoSuchElementException e) {
	    	   System.out.println("The element was not clicked"); 
	       }
	   }
	   
	   public void take_screen() throws Exception
	   {
		 //Convert web driver object to TakeScreenshot
		 TakesScreenshot scrShot =((TakesScreenshot)driver);
		 //Call getScreenshotAs method to create image file
		 File srcFile=scrShot.getScreenshotAs(OutputType.FILE);
		 //Move image file to new destination
		 File destFile=new File("screenshots/" + System.currentTimeMillis()+ ".png");
		 //Copy file at destination
		 FileUtils.copyFile(srcFile, destFile);
		 System.out.println("Take screen with sucess");	 
		 
	   }
	   
	   public void assert_Not_Equals(String v1,String v2) throws InterruptedException
		{	
		    //Assertions.assertNotEquals(v1,v2);
			System.out.println("The text are equals");
			Thread.sleep(2500);
		}
	   
	   public void assert_equals(String v1,String v2) throws InterruptedException
		{	
		   	//Assertions.assertEquals(v1,v2);
			System.out.println("The text are equals");
			Thread.sleep(2500);
		}
	   
	   public void scroll(String scl) throws InterruptedException
	    {
	        	JavascriptExecutor js = (JavascriptExecutor)driver;
	        	js.executeScript("window.scrollBy(0," + scl + ")");
	        	Thread.sleep(1000);
	            System.out.println("Scroll com sucesso");
	    }
	   
	   public boolean is_displayed(String type,String value) {
		   try {
			   WebElement elem;
			   elem = get_element(type,value);
			   elem.isDisplayed();
			   System.out.println("The element is displayed");
			   return true;
		   }
		 catch (org.openqa.selenium.NoSuchElementException e) {
			 System.out.println("The element is NOT displayed");
		     return false;
		   }
		 }
	   
	   public boolean assert_true(Boolean ele) {
		   try {
			   //Assertions.assertTrue(ele);
			   System.out.println("The element is displayed");
			   return true;
		   }
		 catch (org.openqa.selenium.NoSuchElementException e) {
			 System.out.println("The element is NOT displayed");
		     return false;
		   }
		 }
	   
	   public void actionPerform(String type,String value,String i) throws InterruptedException {
		    Actions actions = new Actions(this.driver);
			WebElement element = get_element(type,value); 
			actions.moveToElement(element).perform();
			scroll(i);
			Thread.sleep(1000);
		 }
	   
	   public void actionPerformClick(String type,String value) throws InterruptedException {
		    Actions actions = new Actions(this.driver);
			WebElement element = get_element(type,value); 
			Actions elem = actions.moveToElement(element);
			elem.click().perform(); //contextclik
			Thread.sleep(1000);
		 }
	   
	   public void alertAccept()
	   {
		   this.driver.switchTo().alert().accept();
	   }
	   
	   public void alertDismiss()
	   {
		   this.driver.switchTo().alert().dismiss();
	   }

	   public void alertText(String message)
	   {
		   this.driver.switchTo().alert().sendKeys(message);;
	   }
	   
	   public void switchFrame(WebElement element)
	   {
		   this.driver.switchTo().frame(element);
	   }
}
